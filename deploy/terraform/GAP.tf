
resource "google_app_engine_standard_app_version" "weatherapp" {
  version_id = "v1"
  service    = "weatherapp"
  runtime    = "go115"
  project    = "apavanello-labs"

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.weatherapp.name}/${google_storage_bucket_object.weatherapp.name}"
    }
  }

  env_variables = {
    DBHost=google_compute_instance.mongoDB.network_interface[0].access_config[0].nat_ip
    DBPort="27017"
  }

  delete_service_on_destroy = true

  depends_on = [google_compute_instance.mongoDB, google_compute_firewall.MongoFW, google_storage_bucket_object.weatherapp]
}
