terraform {
  required_version = ">= 0.12"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
  backend "gcs" {
    bucket      = "apavanello-terraform-state"
    prefix      = "terraform/weatherApp/state"
  }
}

provider "google" {
  alias       = "east"
  project     = "apavanello-labs"
  region      = "us-east1"
  zone        = "us-east1-c"
}
