resource "google_compute_firewall" "MongoFW" {
  name     = "mongo-firewall"
  network  = "default"
  provider = google.east

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["27017", "22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["mongodb"]
}