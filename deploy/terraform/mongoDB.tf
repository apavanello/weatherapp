data "google_compute_image" "debian-9" {

  provider = google.east
  family  = "debian-10"
  project = "debian-cloud"

}

resource "google_compute_instance" "mongoDB" {
  provider     = google.east
  name         = "mongodb"
  hostname     = "mongo.apavanello.tk"
  machine_type = "f1-micro"
  zone         = "us-central1-a"

  tags = ["mongodb"]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian-9.name

    }
  }
  metadata = {
    ssh-keys = "root:${file("./files/mongo_ci_ed25519.pub")}"
  }
  network_interface {
    network = "default"


    access_config {
    }

  }
}

output "mongodb_IP" {
  value = google_compute_instance.mongoDB.network_interface[0].access_config[0].nat_ip
}