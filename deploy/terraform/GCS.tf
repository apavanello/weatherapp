resource "google_storage_bucket" "weatherapp" {
  name          = "apavanello-weatherapp"
  location      = "US-CENTRAL1"
  force_destroy = true
  project    = "apavanello-labs"

}

resource "google_storage_bucket_object" "weatherapp" {
  name   = "source-app-engine.${data.archive_file.weatherapp-source-zip.output_md5}.zip"
  source = data.archive_file.weatherapp-source-zip.output_path
  bucket = google_storage_bucket.weatherapp.name
}

data "archive_file" "weatherapp-source-zip" {
  type        = "zip"
  source_dir  = "../../cmd"
  output_path = "../../bin/app.zip"
}