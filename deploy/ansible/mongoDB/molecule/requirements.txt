docker==5.0.0
molecule==3.3.4
molecule-docker==0.2.4
pytest==6.2.4
pytest-testinfra==6.4.0
pymongo==3.11.4
testinfra==6.0.0
yamllint==1.26.1