package main

import (
	"github.com/gin-gonic/gin"
)

func startServer() {


	router := gin.Default()

	router.GET("/weather/random", getRandomLoc)
	router.GET("/weather/loc", getByLoc)

	router.Run(":8080")

}
