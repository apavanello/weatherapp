package main

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"time"
)

var client *mongo.Client
var err error
var weather Weather
var DBUri string


func loadEnvs () {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	DBUser := os.Getenv("vault_DBAdminUser")
	DBPass := os.Getenv("vault_DBAdminPass")
	DBHost := os.Getenv("DBHost")
	DBPort := os.Getenv("DBPort")
	DBUri = fmt.Sprintf("mongodb://%s:%s@%s:%s", DBUser, DBPass, DBHost, DBPort)
}

func initMongoDB() {

	log.Printf("Initializing MongoDB")
	log.Printf(DBUri)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err = mongo.Connect(ctx, options.Client().ApplyURI(DBUri))
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			log.Fatal(err)
		}
	}()
	ctx, cancel = context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Connected to mongoDB")
}

func insertData(weather Weather) bool {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err = mongo.Connect(ctx, options.Client().ApplyURI(DBUri))
	collection := client.Database("weather").Collection("cache")

	_, err := collection.InsertOne(ctx, weather)
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func getData(LocID string, lat string, lng string) Weather {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err = mongo.Connect(ctx, options.Client().ApplyURI(DBUri))

	collection := client.Database("weather").Collection("cache")
	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	filter := bson.D{primitive.E{Key: "locid", Value: LocID}}
	err := collection.FindOne(ctx, filter).Decode(&weather)

	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			weather, _ := getWeatherDataExternal(lat, lng, LocID)
			return weather
		}
		log.Fatal(err)
	}

	weather.LocID = LocID

	return weather
}
