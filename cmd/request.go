package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func getWeatherDataExternal(lat string, lng string, locID string) (Weather, bool) {

	var weather Weather

	baseURI := fmt.Sprintf("https://www.7timer.info/bin/api.pl?lon=%s&lat=%s&ac=0&unit=metric&output=json&tzshift=0&product=civillight", lng, lat)
	log.Printf(baseURI)
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	resp, err := http.Get(baseURI)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(body, &weather)

	if err != nil {
		weather = Weather{}
		weather.LocID = locID
		weather.Info = "Can't find any data for this location"
	}

	weather.LocID = locID
	weather.Info = fmt.Sprintf("https://www.google.com.br/maps/@%s,%s,15z", lat, lng)
	res := insertData(weather)

	return weather, res

}
