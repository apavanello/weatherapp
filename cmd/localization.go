package main

import (
	"github.com/mmcloughlin/spherand"
	"math/rand"
	"strconv"
)

func ganerateCords() (float64, float64) {

	g := spherand.NewGenerator(rand.New(rand.NewSource(42)))
	lat, lng := g.Geographical()

	return lat, lng

}

func convertLatLon(lat string, lng string) (float64, float64, string) {

	lat64, err := strconv.ParseFloat(lat, 64)
	if err != nil {
		return 0, 0, err.Error()
	}

	lng64, err := strconv.ParseFloat(lng, 64)
	if err != nil {
		return 0, 0, err.Error()
	}

	if (lat64 < -90) || (lat64 > 90) {
		errMsg := "latitude out of index range [-90.00, 90.00]"
		return 0, 0, errMsg
	}

	if (lng64 < -180) || (lng64 > 180) {
		errMsg := "Longitude out of index range [-180.00, 180.00]"
		return 0, 0, errMsg
	}

	return lat64, lng64, ""

}
