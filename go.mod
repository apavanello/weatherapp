module main

go 1.15

require (
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/gin-gonic/gin v1.7.2
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/btree v1.0.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mmcloughlin/globe v0.0.0-20200201185603-653bb586373c // indirect
	github.com/mmcloughlin/spherand v0.0.0-20200201191112-cd5c4c9261aa
	github.com/tidwall/pinhole v0.0.0-20210130162507-d8644a7c3d19 // indirect
	go.mongodb.org/mongo-driver v1.5.3
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
)
