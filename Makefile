.PHONY: all help

## Exibe help
all: help


## Create local developer environment
dev:
	cd build && docker-compose up --build -d

go-test:
	go mod tidy \
	&& go vet ./...

load-envs-local:
	ansible-vault decrypt --output cmd/.env --vault-password-file /home/alpsilva/PRJ/ESTUDO/sec/.vault_pass deploy/ansible/mongoDB/vars/vault.yml


load-envs:
	ansible-vault decrypt --output cmd/.env --vault-password-file .vault.key deploy/ansible/mongoDB/vars/vault.yml
##
deploy-all: copy-mod-app load-envs terraform-deploy ansible-mongodb-deploy

## Initialize terraform
terraform-quality:
	cd deploy/terraform && terraform validate

terraform-init:
	cd deploy/terraform && terraform init -upgrade

terraform-plan:
	cd deploy/terraform && terraform plan -out plan.tfplan

terraform-apply:
	cd deploy/terraform && terraform apply "plan.tfplan"

terraform-deploy: terraform-init terraform-plan terraform-apply terraform-export-mongodb-ip

terraform-destroy:
	cd deploy/terraform && terraform destroy -auto-approve

ansible-pre-install:
	cd   deploy/ansible/mongoDB \
	&& python3 -m venv .venv \
	&& . .venv/bin/activate \
	&& pip3 install -r requirements.txt \
	&& ansible-galaxy install -r requirements.yml

terraform-export-mongodb-ip:
	cd deploy/terraform \
	&& terraform refresh \
	&& echo "[mongodb]" > ../ansible/mongoDB/files/inventory \
	&& terraform output -raw mongodb_IP >> ../ansible/mongoDB/files/inventory

ansible-test-partial-mongodb: ansible-test-destroy-mongodb ansible-test-module-mongodb ansible-test-destroy-mongodb

ansible-test-full-mongodb: ansible-test-destroy-mongodb ansible-test-module-mongodb ansible-test-module-mongodb ansible-test-destroy-mongodb

ansible-test-module-mongodb:
	cd deploy/ansible/mongoDB \
	&& . .venv/bin/activate \
	&& molecule create	\
	&& molecule converge  \
	&& molecule verify

ansible-test-destroy-mongodb: ansible-pre-install
	cd deploy/ansible/mongoDB \
	&& . .venv/bin/activate \
	&& molecule destroy

ansible-mongodb-deploy:
	cd deploy/ansible \
	&& ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i mongoDB/files/inventory --private-key "../../.ssh.key" --vault-password-file "../../.vault.key" mongo.yml

copy-mod-app:
	cp go.mod cmd/

# region para montar o help
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)


TARGET_MAX_CHAR_NUM=20
## Exibe help
help:
	@echo ''
	@echo 'Uso:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

